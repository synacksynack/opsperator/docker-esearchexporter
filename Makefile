SKIP_SQUASH?=1
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	@@SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: clean
clean:
	@@docker rm -f testes || true
	@@docker rm -f testesxp || true

.PHONY: testes
testes:
	@@docker rm -f testes || true
	@@sysctl -w vm.max_map_count=262144 || echo wamp wamp
	@@docker run --name testes \
	    -e HOSTNAME=es-0 -d testes

.PHONY: testesok
testesok:
	@@if ! docker exec testes /bin/sh -c 'wget -O- http://127.0.0.1:9200/ && echo OK' | grep OK; then \
	    echo ES KO; \
	    exit 1; \
	fi; \
	echo ES OK

.PHONY: test
test:
	@@if ! make testesok; then \
	    make testes; \
	    sleep 120; \
	fi
	@@docker rm -f testesxp || true
	@@esip=`docker inspect testes | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testesxp \
	    -p 9113:9113 \
	    -d testesxp \
	    --es.all \
	    --es.cluster_settings \
	    --es.indices \
	    --es.shards \
	    --es.snapshots \
	    --es.uri=http://$$esip:9200

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in service statefulset; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc create -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "ELASTICSEARCH_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "ELASTICSEARCH_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc create -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc create -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
