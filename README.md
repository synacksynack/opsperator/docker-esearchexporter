# k8s Prometheus ElasticSearch Exporter

Originally forked from https://github.com/justwatchcom/elasticsearch_exporter,
then moved to https://github.com/jessestuart/elasticsearch_exporter for arm
support.

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```
