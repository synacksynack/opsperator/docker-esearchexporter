FROM docker.io/golang:1.16 as builder

# ElasticSearch Exporter image for OpenShift Origin

ENV GO111MODULE on

WORKDIR /go/src/github.com/justwatchcom/elasticsearch_exporter
COPY config/collector ./collector
COPY config/fixtures ./fixtures
COPY config/pkg ./pkg
COPY config/*.go config/*.mod config/*.sum config/Makefile config/VERSION config/.promu.yml ./

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && make promu \
    && make build \
    && cp ./elasticsearch_exporter /

FROM scratch

LABEL io.k8s.description="ElasticSearch Prometheus Exporter Image." \
      io.k8s.display-name="ElasticSearch Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,elasticsearch" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-esearchexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.1"

COPY --from=builder /elasticsearch_exporter /elasticsearch_exporter

ENTRYPOINT ["/elasticsearch_exporter"]
USER 1001
